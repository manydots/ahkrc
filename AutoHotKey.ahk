; Adapted from https://github.com/nathanpeck/autohotkey-windows-10-apple-magic-keyboard/blob/master/AutoHotKey.ahk
;
; --------------------------------------------------------------
; NOTES
; --------------------------------------------------------------
; ! = ALT
; ^ = CTRL
; + = SHIFT
; # = WIN

; --------------------------------------------------------------
; OS X system shortcuts
; --------------------------------------------------------------
;
; map the cmd => alt

; Special case for Ctrl-C and Ctrl-V

#IfWinActive ahk_class mintty
    ; mintty use Ctrl-Insert, Shift-Insert
    !c::SendInput ^{Insert}
    !v::SendInput +{Insert}
#IfWinActive
    ; other app use Ctrl-C, Ctrl-V
    !c::SendInput ^{c}
    !v::SendInput ^{v}
#IfWinActive

!a::SendInput ^{a}
!f::SendInput ^{f}  ; Firefox | search
!l::SendInput ^{l}  ; Firefox | address bar
!n::SendInput ^{n}
!s::SendInput ^{s}
!t::SendInput ^{t}  ; Firefox | new Tab

; Special case for Cmd-W
#IfWinActive ahk_exe code.exe
    !w::SendInput ^{F4}
#IfWinActive 
    !w::SendInput ^{w}
#IfWinActive 

!x::SendInput ^{x}
!z::SendInput ^{z}
!+p::SendInput ^+{p}  ; Firefox | Private Window

!=::SendInput ^{+}  ; zoom in
!-::SendInput ^{-}  ; zoom out


; Close windows (cmd + q to Alt + F4)
!q::Send !{F4}

; minimize windows
!m::WinMinimize,a

; -------------------------------------------------------------- 
; Special mapping for developers
; --------------------------------------------------------------

CapsLock::Escape